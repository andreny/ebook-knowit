﻿app.service('userService', function () {
    var displayName;
    var isLoggedIn;

    this.setDisplayName = function (name) {
        displayName = name;
    };

    this.getDisplayName = function () {
        return displayName;
    };

    this.setLoggedInStatus = function (status) {
        isLoggedIn = status;
    };

    this.getLoggedInStatus = function () {
        return isLoggedIn;
    };
});