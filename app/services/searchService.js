﻿app.service('searchService', function () {
    var queryString = "";

    this.saveQuery = function (query) {
        queryString = query;
    };

    this.getQuery = function () {
        return queryString;
    };
});